import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager

# tạo webdriver
driver = webdriver.Chrome(ChromeDriverManager().install())

# link dẫn tới trang web
driver.get ("https://www.google.com/")

# nhập vào thanh search
search = driver.find_element_by_xpath("//input[@title='Tìm kiếm']")
search.send_keys("python selenium tutorial")
search.send_keys(Keys.ENTER)

#
a = driver.find_elements_by_xpath("//*[@jsname='ARU61']")
print(len(a))
for x in range(0,int(len(a))):
    a[x].click()
    time.sleep(1)
time.sleep(3)

# Tìm và print ra title và link web
ID_rso = driver.find_elements_by_xpath("//div[@class = 'yuRUbf']//h3[@class='LC20lb DKV0Md']")
ID_rso1 = driver.find_elements_by_xpath("//div[@class='yuRUbf']/a")
print(len(ID_rso))
for i in range(0,20):
    if (ID_rso[i].text != ""):
        print("-------------")
        print(str(i+1) + "./" )
        print(ID_rso[i].text)
        print(ID_rso1[i].get_attribute("href"))
        time.sleep(1)

driver.quit()